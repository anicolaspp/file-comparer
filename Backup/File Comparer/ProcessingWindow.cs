﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

using Nikos.Collections;
using Nikos.Extensions.Collections;

namespace File_Comparer
{
    public partial class ProcessingWindow : Form
    {
        public ProcessingWindow(FileUtils tool)
        {
            InitializeComponent();

            this.tool = tool;

            CheckForIllegalCrossThreadCalls = false;

            progressBar1.Maximum = tool.FilesCount;
            progressBar1.Value = 0;

            tool.ProgressReport += tool_ProgressReport;
            tool.EqualFilesFound += tool_EqualFilesFound;
            tool.OnFinished += tool_OnFinished;
            tool.OnGrupChange += delegate
                                     {
                                         grups.Add(new List<string>());
                                         cGrups++;
                                     };
        }

        private FileUtils tool;
        private int cGrups = -1, count;
        private List<List<string>> grups = new List<List<string>>();

        private void tool_OnFinished(object sender, EventArgs e)
        {
            if (!Manual)
            {
                Opacity = 0;
                var thread = new Thread(() => (new WatingWindow { TextLabel = Eliminar ? "Deleting" : "Moving" }).ShowDialog());
                thread.Start();

                #region copy or move region

                if (Eliminar)
                {
                    foreach (var grup in grups)
                        for (int i = 1; i < grup.Count; i++)
                            if (File.Exists(grup[i]))
                                FileUtils.DeleteFile(grup[i]);
                }
                else
                {
                    var path = tool.Root + @"\Grupos";
                    Directory.CreateDirectory(path);
                    int count = 0;

                    foreach (var grup in grups)
                    {
                        int t = 0;
                        if (grup.Count > 0)
                        {
                            var folder = Directory.CreateDirectory(path + @"\" + count).FullName;
                            foreach (var file in grup)
                            {
                                var data = GetFileName(file);
                                FileUtils.CopyFile(file, folder + @"\" + data[0] + "(" + t++ + ")." + data[1]);
                            }
                            count++;
                        }
                    }
                }

                #endregion

                thread.Abort();
            }
            else
            {
                Opacity = 0;
                foreach (var grup in grups)
                {
                    if (grup.Count > 0)
                    {
                        var form4 = new ManualWindow { Items = grup };
                        if (form4.ShowDialog() == DialogResult.Cancel) break;
                    }
                }
            }
            Close();
        }

        private string[] GetFileName(string file)
        {
            string result = "";
            var temp = "";
            for (int i = file.Length - 1; i >= 0; i--)
            {
                if (file[i] == @"\"[0]) break;
                if (file[i] == '.')
                {
                    result = temp.Reverse().C_ToString();
                    temp = "";
                }

                temp += file[i];
            }

            return new[] { temp.Reverse().C_ToString().Remove(temp.Length - 1), result };
        }

        private void tool_EqualFilesFound(object sender, Pair<string, string> args)
        {
            if (cGrups >= grups.Count)
                grups.Add(new List<string>());
            if (!grups[cGrups].Contains(args.Key))
                grups[cGrups].Add(args.Key);
            if (!grups[cGrups].Contains(args.Value))
                grups[cGrups].Add(args.Value);
        }

        private void tool_ProgressReport(object sender, TProgressReport report)
        {
            progressBar1.Value = report.QueueNumber;
            label2.Text = report.FileName_1;
            label3.Text = report.FileName_2;

            string value = (report.QueueNumber * 100 / report.TotalFiles).ToString();
            label1.Text = value + "%";
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            tool.CancelProcess();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (count > 2)
                count = 0;
            else
            {
                string[] a = { ".", "..", "..." };
                label4.Text = "Comparing" + a[count++];
            }
        }

        public new void ShowDialog()
        {
            timer1.Enabled = true;
            tool.ProcessFiles();
            base.ShowDialog();
        }

        public bool Eliminar { get; set; }
        public bool Manual { get; set; }
    }
}
