﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Windows.Forms;
using Nikos.Collections;

namespace File_Comparer
{
    public delegate void ProgressReportEventHandler(object sender, TProgressReport args);
    public delegate void EqualFilesFoundEventHandler(object sender, Pair<string, string> args);

    public class FileUtils
    {
       
        private Nikos.Collections.Dictionary<long, List<string>> files;
        private int cFiles;
        private Thread t;

        public event EqualFilesFoundEventHandler EqualFilesFound;
        public event ProgressReportEventHandler ProgressReport;
        public event EventHandler OnStarProcess;
        public event EventHandler OnFinished;
        public event EventHandler OnGrupChange;

        private void InvokeOnGrupChange(EventArgs e)
        {
            EventHandler onGrupChangeHandler = OnGrupChange;
            if (onGrupChangeHandler != null) onGrupChangeHandler(this, e);
        }
        private void InvokeOnFinished(EventArgs e)
        {
            EventHandler onFinishedHandler = OnFinished;
            if (onFinishedHandler != null) onFinishedHandler(this, e);
        }
        private void InvokeOnStarProcess(EventArgs e)
        {
            EventHandler onStarProcessHandler = OnStarProcess;
            if (onStarProcessHandler != null) onStarProcessHandler(this, e);
        }
        private void InvokeProgressReport(TProgressReport args)
        {
            ProgressReportEventHandler Handler = ProgressReport;
            if (Handler != null) Handler(this, args);
        }
        private void InvokeEqualFilesFound(Pair<string, string> args)
        {
            EqualFilesFoundEventHandler Handler = EqualFilesFound;
            if (Handler != null) Handler(this, args);
        }

        private static bool File_Comparer(string p, string p_2)
        {
            var x = new StreamReader(p);
            var y = new StreamReader(p_2);

            while (!x.EndOfStream)
            {
                var t = x.ReadLine();
                var k = y.ReadLine();

                if (t != k)
                {
                    x.Close();
                    x.Dispose();
                    y.Close();
                    y.Dispose();
                    return false;
                }
            }

            x.Close();
            x.Dispose();
            y.Close();
            y.Dispose();

            return true;
        }
        private static bool AdvancedComparerAlgoritm(string p, string p_2)
        {
            MD5Cng md5Cng = new MD5Cng();
            var x = md5Cng.ComputeHash(new FileStream(p, FileMode.Open));
            var y = md5Cng.ComputeHash(new FileStream(p_2, FileMode.Open));

            if (x.Length != y.Length) return false;

            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] != y[i]) return false;
            }

            return true;
        }

        public static void CopyFile(string source, string dest)
        {
            Thread thread = new Thread(delegate()
                                           {
                                               if (!File.Exists(source)) return;
                                               File.Copy(source, dest, true);
                                           });
            thread.Start();
            thread.Join();
        }
        public static void MoveFile(string source, string dest)
        {
            Thread thread = new Thread(delegate()
                                           {
                                               if (!File.Exists(source)) return;
                                               File.Move(source, dest);
                                           });
            thread.Start();
            thread.Join();
        }
        public static bool DeleteFile(string source)
        {
            FileInfo fileInfo = new FileInfo(source);
            if (fileInfo.Exists)
            {
                fileInfo.IsReadOnly = false;
                fileInfo.Delete();
                return true;
            }
            return false;
        }

        private bool InvokeMethodComparer(string p, string p_2)
        {
            if (AnalisysType == global::File_Comparer.AnalisysType.Exaustive)
                return File_Comparer(p, p_2);
            else
                return AdvancedComparerAlgoritm(p, p_2);
        }
        private void Execute()
        {
            var keys = files.Keys.ToArray();
            int fileNumber = 0;

            for (int i = 0; i < keys.Length; i++)
            {
                fileNumber++;
                InvokeOnGrupChange(new EventArgs());

                if (files[keys[i]].Count == 1)
                    InvokeProgressReport(new TProgressReport {FileName_1 = files[keys[i]][0], QueueNumber = fileNumber, TotalFiles = cFiles});
                else
                {
                    for (int j = 0; j < files[keys[i]].Count - 1; j++)
                    {
                        for (int k = j + 1; k < files[keys[i]].Count; k++)
                        {
                            InvokeProgressReport(new TProgressReport {FileName_1 = files[keys[i]][j], FileName_2 = files[keys[i]][k], QueueNumber = fileNumber, TotalFiles = cFiles});
                            if (InvokeMethodComparer(files[keys[i]][j], files[keys[i]][k]))
                            {
                                InvokeEqualFilesFound(new Pair<string, string> {Key = files[keys[i]][j], Value = files[keys[i]][k]});
                            }
                        }
                        fileNumber++;
                    }
                }
            }

            InvokeOnFinished(new EventArgs());
        }
        private void ListFiles(DirectoryInfo root)
        {
            try
            {
                foreach (var file in root.GetFiles())
                {
                    cFiles++;
                    if (files.ContainsKey(file.Length))
                    {
                        files[file.Length].Add(file.FullName);
                    }
                    else
                    {
                        files.Add(file.Length, new List<string> { file.FullName });
                    }
                }

                foreach (var directory in root.GetDirectories())
                {
                    ListFiles(directory);
                }
            }
            catch (UnauthorizedAccessException)
            {
            }
        }

        public void ListFiles()
        {
            ListFiles(Root);
        }
        public FileUtils(DirectoryInfo root)
        {
            AnalisysType = global::File_Comparer.AnalisysType.Exaustive;
            Root = root;
            files = new Nikos.Collections.Dictionary<long, List<string>>();
        }
        
        public void ProcessFiles()
        {
            InvokeOnStarProcess(new EventArgs());
            t = new Thread(Execute);
            t.Start();
        }
        public void CancelProcess()
        {
            if (t.ThreadState == ThreadState.Running)
                t.Abort();
            t = null;
        }

        public IEnumerable<string> Files
        {
            get
            {
                foreach (var pair in files)
                {
                    foreach (var item in pair.Value)
                    {
                        yield return item;
                    }
                }
            }
        }
        public DirectoryInfo Root { get; protected set; }
        public int FilesCount { get { return cFiles; } }
        public AnalisysType AnalisysType { get; set; }
    }

    public enum AnalisysType
    {
        Exaustive = 0, Probabilistic = 1
    }

    public class TProgressReport
    {
        public int QueueNumber { get; set; }
        public int TotalFiles { get; set; }
        public string FileName_1 { get; set; }
        public string FileName_2 { get; set; }
    }
}
