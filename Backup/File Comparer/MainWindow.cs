﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace File_Comparer
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();

            button2.Enabled = active;
        }

        private FileUtils tool;
        private bool active;

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                active = true;
                tool = new FileUtils(new DirectoryInfo(folderBrowserDialog1.SelectedPath));

                var thread = new Thread(() => (new WatingWindow { TextLabel = "listando", DesktopLocation = DesktopLocation }).ShowDialog());

                thread.Start();
                tool.ListFiles();

                foreach (var file in tool.Files)
                    listBox1.Items.Add(file);

                thread.Abort();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Count == 0)
                MessageBox.Show("No se ha seleccionado un directorio", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
            else
            {
                tool.AnalisysType = radioButton3.Checked ? AnalisysType.Exaustive : AnalisysType.Probabilistic;
                var form2 = new ProcessingWindow(tool) { Eliminar = (radioButton2.Checked ? true : false), ShowInTaskbar = false, Manual = checkBox1.Checked };
                form2.ShowDialog();
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                active = button2.Enabled = true;
                radioButton1.Enabled = radioButton2.Enabled = false;
                radioButton1.Checked = radioButton2.Checked = false;
            }
            else
            {
                active = button2.Enabled = false;
                radioButton1.Enabled = radioButton2.Enabled = true;
            }
        }
    }
}
