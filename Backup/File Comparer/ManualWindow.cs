﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace File_Comparer
{
    public partial class ManualWindow : Form
    {
        public ManualWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var selectedItem in listBox1.SelectedItems)
                FileUtils.DeleteFile(selectedItem.ToString());
            
            DialogResult = DialogResult.OK;
            Close();
        }

        public IEnumerable<string> Items
        {
            set 
            {
                foreach (var item in value)
                    listBox1.Items.Add(item);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Ignore;
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
